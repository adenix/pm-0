********************************************************************************
** Author: Austin Nicholas
**         DaQueshia Irvin
**         Mario Carrasco
**
** PM/0 Virtual Machine -- README.txt
********************************************************************************

### RUNNING PM/0 SCANNER

To run vm.c use the following syntax
   >   gcc scanner.c
   >   ./a.out [INPUT]
   *   [INPUT] -  text file formated to contain three numbers separated by a
                  space on each line.

### DEBUG MODE

Debug mode will print out processes that are happening during execution in the
following format:

> RUNNING: Clean Input
  > Removing \r
  > Removing \r
  > Removing Comment: '/* Comment */'
  > Removing \r
  > Removing \r
  > Removing \r
 > RUNNING: Scanner
  > Alpha Token: 'var' 29
  > Alpha Token: 'x' 2
  > Other Token: ',' 17
  > Alpha Token: 'y' 2
  > Other Token: ';' 18
  > Alpha Token: 'begin' 21
  > Alpha Token: 'y' 2
  > Other Token: ':=' 20
  > Digit Token: '3' 3
  > Other Token: ';' 18
  > Alpha Token: 'x' 2
  > Other Token: ':=' 20
  > Alpha Token: 'y' 2
  > Other Token: '+' 4
  > Digit Token: '56' 3
  > Other Token: ';' 18
  > Alpha Token: 'end' 22
  > Other Token: '.' 19

At the top of the 'Defines' section in vm.c there is the following line of code:
   13 // DEBUG
   14  #define DEBUG 0

Change the '0' to a '1' to run the program in Debug mode.
