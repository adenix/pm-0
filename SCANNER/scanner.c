/*******************************************************************************
 ** Author: Austin Nicholas
 **         DaQueshia Irvin
 **         Mario Carrasco
 **
 ** PM/0 Scanner -- scanner.c v3.4.0
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// -----[ Defines ]-------------------------------------------------------------
// DEBUG
#define DEBUG 1

// dummy bool data type
typedef int bool;
#define true  1
#define false 0

#define MAX_IDENTIFIERS 11
#define MAX_NUMBERS 5
#define DIGIT_ERR 1 //(Mario 10/18)
#define ALPHA_ERR 2
#define INVALID_VAR_ERR 3
#define INVALID_SYM_ERR 4

// Command line color codes
#define NRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define YEL  "\x1B[33m"
#define BLU  "\x1B[34m"
#define MAG  "\x1B[35m"
#define CYN  "\x1B[36m"
#define WHT  "\x1B[37m"

// -----[ Global Variables ]----------------------------------------------------
int lineCount = 1; //For printing error messages (Mario 10/18)

FILE* in;
FILE* cleaninput;
FILE* lexemetable;
FILE* tokenlist;

typedef struct {
    char* name;
    int value;
} token;

typedef enum {
    nulsym = 1, identsym, numbersym, plussym, minussym, multsym,  slashsym,
    oddsym, eqsym, neqsym, lessym, leqsym, gtrsym, geqsym, lparentsym,
    rparentsym, commasym, semicolonsym, periodsym, becomessym, beginsym,
    endsym, ifsym, thensym, whilesym, dosym, callsym, constsym, varsym, procsym,
    writesym, readsym, elsesym
} token_type;

// -----[ verifyArgs ]----------------------------------------------------------
// Description:     Takes in arguments from the command line and verfies that
//                  the arguments are correctly formatted.
//
// Returns:         True if the arguments are formated correctly. False if not
//
bool verifyArgs(int argc, char **argv) {

    if(argc == 2) {

        if(!strcmp(argv[1], "--help")) {

            printf("\n");
            printf(
              "%sPM/0: %sTo run scanner.c use the following syntax\n\n",
                   YEL, NRM);
            printf(
              "   >%s   ./a.out [INPUT]\n%s", GRN, NRM);
            printf(
              "   *   [INPUT] - text file formated to contain the input.\n%s",
                   NRM);;

            printf("\n\n");

            return false;

        }
    } else {

        printf("\n");
        printf("%sERROR: %sCommand line arguments syntax is incorrect\n",
               YEL, NRM);
        printf("       Please retry with the following format..\n\n");
        printf("   >%s   ./a.out [INPUT]\n%s", GRN, NRM);
        printf("   *   for help run './a.out --help'");

        printf("\n\n");

        return false;
    }

    return true;
}

// -----[ errorFound ]----------------------------------------------------------
// Description:     Produces an error message and halts.
//
// Return:          Void.

void errorFound(int err)
{
    switch(err)
    {

        case DIGIT_ERR:
        printf(
          ">> ERROR -- Line %d: Max digit length has been exceeded.\n\n",
          lineCount);
        break;

        case ALPHA_ERR:
        printf(
          ">> ERROR -- Line %d: Max identifier length has been exceeded.\n\n",
          lineCount);
        break;

        case INVALID_VAR_ERR:
        printf(
          ">> ERROR -- Line %d: Invalid identifier.\n\n", lineCount);
        break;

        case INVALID_SYM_ERR:
        printf(
          ">> ERROR -- Line %d: Invalid symbol.\n\n", lineCount);
        break;

        default:
        printf(
          ">> ERROR -- Line %d: Unknown error.\n\n", lineCount);

    }

    exit(EXIT_FAILURE);

}

// -----[ alphaToken ]----------------------------------------------------------
// Description:     Reads in a series of characters, and creates a token for the
//                  symbol.
//                  * Produces an error and exits program if identifier length
//                    is exceeded.
//
// Return:          A token with either a literal or identifer with the
//                  appropiate value.
token alphaToken(FILE* in, char currentChar) {

    token temp;
    temp.name = (char*)calloc(MAX_IDENTIFIERS+1, sizeof(char));
    //                        ^ '+1' for the null char - (Mario 10/18)

    int i = 0;

    while(currentChar != EOF && (isalpha(currentChar) ||
      isdigit(currentChar))) {

        temp.name[i++] = currentChar;
        currentChar = fgetc(in);

        //Error --Max identifier length exceeded. (Mario 10/18)
        if(i > MAX_IDENTIFIERS) errorFound(ALPHA_ERR);
    }
    temp.name[i] = '\0';

    if(currentChar != EOF) fseek(in, -1, SEEK_CUR);

    if(!strcmp(temp.name, "odd"))
        temp.value = oddsym;
    if(!strcmp(temp.name, "begin"))
        temp.value = beginsym;
    else if(!strcmp(temp.name, "end"))
        temp.value = endsym;
    else if(!strcmp(temp.name, "if"))
        temp.value = ifsym;
    else if(!strcmp(temp.name, "then"))
        temp.value = thensym;
    else if(!strcmp(temp.name, "while"))
        temp.value = whilesym;
    else if(!strcmp(temp.name, "do"))
        temp.value = dosym;
    else if(!strcmp(temp.name, "call"))
        temp.value = callsym;
    else if(!strcmp(temp.name, "const"))
        temp.value = constsym;
    else if(!strcmp(temp.name, "var"))
        temp.value = varsym;
    else if(!strcmp(temp.name, "procedure"))
        temp.value = procsym;
    else if(!strcmp(temp.name, "write"))
        temp.value = writesym;
    else if(!strcmp(temp.name, "read"))
        temp.value = readsym;
    else if(!strcmp(temp.name, "else"))
        temp.value = elsesym;
    else
        temp.value = identsym;

    if(DEBUG) printf("  > Alpha Token: '%s' %d\n", temp.name, temp.value);

    return temp;
}

// -----[ digitToken ]----------------------------------------------------------
// Description:     Reads in a digit, and creates a token for the symbol.
//                  * Produces an error and exits program if digit length is
//                    exceeded.
//
// Return:          Token with the digit (char *) and numbersym value.
token digitToken(FILE* in, char currentChar) {

    token temp;
    temp.name = (char*)calloc(MAX_NUMBERS+1, sizeof(char));
    //                        ^ '+1' for the null char (Mario 10/18)

    int i = 0;

    while(currentChar != EOF && isdigit(currentChar)) {

        temp.name[i++] = currentChar;
        currentChar = fgetc(in);

        //Error --max digit length exceeded. (Mario 10/18)
        if(i > MAX_NUMBERS) errorFound(DIGIT_ERR);
    }
    //Error -- Invalid variable name.
    if(isalpha(currentChar))
        errorFound(INVALID_VAR_ERR);

    temp.name[i] = '\0';

    if(currentChar != EOF) fseek(in, -1, SEEK_CUR);

    temp.value = numbersym;

    if(DEBUG) printf("  > Digit Token: '%s' %d\n", temp.name, temp.value);

    return temp;
}

// -----[ otherToken ]----------------------------------------------------------
// Description:     A function for identifying arithmetic operators, comparison
//                  operators, and comments. Reads in currentChar and produces a
//                  token.
//
// Return:          Token with the symbol and appropriate value.
token otherToken(FILE* in, char currentChar) {

    bool firstChar = true;
    token temp;
    temp.name = (char*)calloc(5, sizeof(char));

    int i = 0;

    while(currentChar != EOF && (!isalpha(currentChar)) &&
      (!isdigit(currentChar)) && currentChar != ' ' && currentChar != '\n') {

        if(currentChar == ';') {
          if(!firstChar) break;
        }

        temp.name[i++] = currentChar;
        currentChar = fgetc(in);
        firstChar = false;
    }
    temp.name[i] = '\0';

    for(i = 0; i < strlen(temp.name); i++) {
        if(temp.name[i] == '\n')
            temp.name[i] = '\0';
    }

    if(currentChar != EOF) fseek(in, -1, SEEK_CUR);

    if(!strcmp(temp.name, " "))
        temp.value = nulsym;
    else if(!strcmp(temp.name, "+"))
        temp.value = plussym;
    else if(!strcmp(temp.name, "-"))
        temp.value = minussym;
    else if(!strcmp(temp.name, "*"))
        temp.value = multsym;
    else if(!strcmp(temp.name, "/"))
        temp.value = slashsym;
    else if(!strcmp(temp.name, "="))
        temp.value = eqsym;
    else if(!strcmp(temp.name, "<>"))
        temp.value = neqsym;
    else if(!strcmp(temp.name, "<"))
        temp.value = lessym;
    else if(!strcmp(temp.name, "<="))
        temp.value = leqsym;
    else if(!strcmp(temp.name, ">"))
        temp.value = gtrsym;
    else if(!strcmp(temp.name, ">="))
        temp.value = geqsym;
    else if(!strcmp(temp.name, "("))
        temp.value = lparentsym;
    else if(!strcmp(temp.name, ")"))
        temp.value = rparentsym;
    else if(!strcmp(temp.name, ","))
        temp.value = commasym;
    else if(!strcmp(temp.name, ";"))
        temp.value = semicolonsym;
    else if(!strcmp(temp.name, "."))
        temp.value = periodsym;
    else if(!strcmp(temp.name, ":="))
        temp.value = becomessym;
    else{
        if(strcmp(temp.name," ")) // An invalid Symbol
            errorFound(INVALID_SYM_ERR);
        else
        temp.value = -1; // set flag for a series of blank spaces.
    }


    if(DEBUG) printf("  > Other Token: '%s' %d\n", temp.name, temp.value);

    return temp;
}

// -----[ main ]----------------------------------------------------------------
int main(int argc, char **argv) {

    char currentChar;

    if(!verifyArgs(argc, argv)) return 0;

    // -----[ File Open ]---------------------------------
    in = fopen(argv[1], "r");
    cleaninput = fopen("cleaninput.txt", "w");
    lexemetable = fopen("lexemetable.txt", "w");
    tokenlist = fopen("tokenlist.txt", "w");

    fprintf(lexemetable, "lexeme\ttoken type\n");

    // -----[ Body ]--------------------------------------
    if(DEBUG) printf(" > RUNNING: Clean Input\n");

    while((currentChar = fgetc(in)) != EOF) {

        if(currentChar == '/') {
            if((currentChar = fgetc(in)) == '*') {
              if(DEBUG) printf("  > Removing Comment: '/*");
                while(true) {
                    if((currentChar = fgetc(in)) == '*') {
                        if((currentChar = fgetc(in)) == '/')
                            break;
                        else
                          if(DEBUG) printf("*%c", currentChar);
                    } else {
                      if(DEBUG) printf("%c", currentChar);
                    }
                }
                if(DEBUG) printf("*/'\n");
            }
            else {
              fprintf(cleaninput, "/%c", currentChar);
            }

        } else if(currentChar == '\r') {
          if(DEBUG) printf("  > Removing \\r\n");
          continue;
        } else {
          fprintf(cleaninput, "%c", currentChar);
        }
    }

    fclose(in);
    fclose(cleaninput);
    cleaninput = fopen("cleaninput.txt", "r");

    if(DEBUG) printf(" > RUNNING: Scanner\n");
    while((currentChar = fgetc(cleaninput)) != EOF) {

        token token;

        if(currentChar == ' ' || currentChar == '\n' || currentChar == '\t'){

            //Increment lineCount for error-message printouts (Mario 10/18)
            if(currentChar == '\n')
                lineCount++;

            continue;
        }
        else if(isalpha(currentChar))
          token = alphaToken(cleaninput, currentChar);
        else if(isdigit(currentChar))
          token = digitToken(cleaninput, currentChar);
        else
          token = otherToken(in, currentChar);

        if(token.value != -1){ // Prevents printing of blank spaces
            fprintf(lexemetable, "%s\t%d\n", token.name, token.value);
        }
        if(token.value == 2 || token.value == 3)
            fprintf(tokenlist, "%d %s ", token.value, token.name);
        else if (token.value != -1)// Prevents printing of blank spaces
            fprintf(tokenlist, "%d ", token.value);
    }

    // -----[ File Close ]--------------------------------
    fclose(cleaninput);
    fclose(lexemetable);
    fclose(tokenlist);

    return 0;
}
