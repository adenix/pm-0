********************************************************************************
** Author: Austin Nicholas
**         DaQueshia Irvin
**         Mario Carrasco
**
** PM/0 PARSER -- README.txt
********************************************************************************

### RUNNING PM/0 PARSER

To run parser.c use the following syntax
   >   gcc parser.c
   >   ./a.out [INPUT]
   *   [INPUT] -  text file formated as a token list

### DEBUG MODE

Debug mode will print out processes that are happening during execution in the
following format:

    DEBUG > FILE: 'Test Cases/Factorial/tokenlist.txt'
    DEBUG > PROGRAM ENTERED
    DEBUG >   Token '28' read
    DEBUG > BLOCK ENTERED
    DEBUG >     EMIT: 7 0 0
    DEBUG >   Token '2' read
    DEBUG >   Token '9' read
    DEBUG >   Token '3' read
    DEBUG >     SYMBOL -> Type: Constant; Name: beginningX; Level: 0; Value: 3;
    DEBUG >   Token '18' read
    DEBUG >   Token '29' read
    DEBUG >   Token '2' read
    DEBUG >     SYMBOL -> Type: Variable; Name: facRes; Level: 0; Address: 4;
    DEBUG >   Token '17' read
    DEBUG >   Token '2' read
    DEBUG >     SYMBOL -> Type: Variable; Name: facParam; Level: 0; Address: 5;
    DEBUG >   Token '18' read
    DEBUG >   Token '30' read
    DEBUG >   Token '2' read
    DEBUG >     SYMBOL -> Type: Procedure; Name: Factorial; Level: 0; Address: 1;
    DEBUG >   Token '18' read
    DEBUG >   Token '29' read
    DEBUG > BLOCK ENTERED
    DEBUG >     EMIT: 7 0 0
    DEBUG >   Token '2' read
    DEBUG >     SYMBOL -> Type: Variable; Name: myFacParam; Level: 1; Address: 4;
    DEBUG >   Token '18' read
    DEBUG >   Token '21' read
    DEBUG >     EMIT: 6 0 5
    [...]
    DEBUG >     EMIT: 11 0 3

At the top of the 'Defines' section in vm.c there is the following line of code:
   18 // DEBUG
   19  #define DEBUG 0

Change the '0' to a '1' to run the program in Debug mode.
