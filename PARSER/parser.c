/*******************************************************************************
 ** Author: Austin Nicholas
 **         DaQueshia Irvin
 **         Mario Carrasco
 **
 ** PM/0 Scanner -- parcer.c v3.5.0
 *******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// -----[ Function Declaration (AS NEEDED) ]------------------------------------
int expression(FILE* in, int token);

// -----[ Defines ]-------------------------------------------------------------
// DEBUG
#define DEBUG 0

// dummy bool data type
typedef int bool;
#define true  1
#define false 0

#define MAX_SYMBOL_TABLE_SIZE 100
#define MAX_CODE_LENGTH 500
#define MAX_STACK_HEIGHT 2000
#define MAX_LEXI_LEVELS 3

// For constants, store kind, name, and value
// For variables, store kind, name, L, and M
// For procedures, store kind, name, L, and M
typedef struct {
    int kind;       // const = 1, var = 2, proc = 3
    char name[12];  // name up to 11 characters
    int val;        // value
    int level;      // L level
    int addr;       // M address
} symbol;

typedef struct {
    int OP;     // Op code
    int L;      // Lex level
    int M;      // Address
} instruction;

// Command line color codes
#define NRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define YEL  "\x1B[33m"
#define BLU  "\x1B[34m"
#define MAG  "\x1B[35m"
#define CYN  "\x1B[36m"
#define WHT  "\x1B[37m"

// -----[ Global Variables ]----------------------------------------------------
typedef enum {
  nulsym = 1, identsym, numbersym, plussym, minussym, multsym,  slashsym,
  oddsym, eqsym, neqsym, lessym, leqsym, gtrsym, geqsym, lparentsym,
  rparentsym, commasym, semicolonsym, periodsym, becomessym, beginsym,
  endsym, ifsym, thensym, whilesym, dosym, callsym, constsym, varsym, procsym,
  writesym, readsym, elsesym
} token_type;

typedef enum {
  constant = 1, variable, procedure
} symbol_type;

typedef enum {
  LIT = 1, OPR, LOD, STO, CAL, INC, JMP, JPC, SIO1, SIO2, SIO3
} op_code;

typedef enum {
  NEG = 1, ADD, SUB, MUL, DIV, ODD, MOD, EQL, NEQ, LSS, LEQ, GTR, GEQ
} relation;

symbol symbol_table[MAX_SYMBOL_TABLE_SIZE];
instruction code[MAX_CODE_LENGTH];
// int lexStackHist[MAX_LEXI_LEVELS];

int symPtr,
    codePtr,
    lexPtr,
    stackPtr;

int procCount;

FILE* in;

void debug(char* msg, char* CLR) {
  printf("%s  DEBUG > %s%s\n", CLR, msg, NRM);
}

// -----[ verifyArgs ]----------------------------------------------------------
// Description:     Takes in arguments from the command line and verfies that
//                  the arguments are correctly formatted.
//
// Returns:         True if the arguments are formated correctly. False if not
//
bool verifyArgs(int argc, char **argv) {

  if(argc == 2) {

    if(!strcmp(argv[1], "--help")) {

      printf("\n");
      printf(
        "%sPM/0: %sTo run scanner.c use the following syntax\n\n",
        YEL, NRM);
      printf(
        "   >%s   ./a.out [INPUT]\n%s", GRN, NRM);
      printf(
        "   *   [INPUT] - text file formated to contain the input.\n%s",
        NRM);

      printf("\n\n");

        return false;

      }
    } else {

      printf("\n");
      printf("%sERROR: %sCommand line arguments syntax is incorrect\n",
      YEL, NRM);
      printf("       Please retry with the following format..\n\n");
      printf("   >%s   ./a.out [INPUT]\n%s", GRN, NRM);
      printf("   *   for help run './a.out --help'");

      printf("\n\n");

      return false;
    }

  return true;
}

// *****[ PARCER PROCEDURES ]***************************************************

// -----[ error ]---------------------------------------------------------------
// Description:     Verbos error message to screen and exit(0)
void error(char* msg){
  printf("ERROR: %s\n", msg);
  exit(0);
}

// -----[ readToken ]-----------------------------------------------------------
// Description:     Reads in next token from 'tokenlist.txt' and displays a
//                  debug message if DEBUG flag is '1'
//
// Returns:         The token retrived from 'tokenlist.txt'
int readToken(FILE* in) {

  int token;

  fscanf(in, "%d", &token);
  if(DEBUG) {
    char buffer[25];
    sprintf(buffer, "  Token '%d' read", token);
    debug(buffer, YEL);
  }

  return token;
}

// -----[ emit ]----------------------------------------------------------------
// Description:     Output PL/0 code
// print the appropriate code to the code file
void emit(int OP, int L, int M)
{
  code[codePtr].OP = OP;
  // code[codePtr].R = R;
  code[codePtr].L = L;
  code[codePtr].M = M;

  if(DEBUG) {
    char buffer[50];
    sprintf(buffer, "    EMIT: %d %d %d",
    code[codePtr].OP, code[codePtr].L, code[codePtr].M);
    debug(buffer, MAG);
  }

  codePtr++;
}

// -----[ findSym ]-------------------------------------------------------------
// Description:     Search symbol table for a specific symbol.
//
// Returns:         A pointer to the symbol if found. Other wise NULL
symbol* findSym(char* name) {

  int i;
  symbol* maxLex = NULL;

  for(i = 0; i < symPtr; i++) {
    if(!strcmp(name, symbol_table[i].name) && (symbol_table[i].level <= lexPtr)) maxLex = &symbol_table[i];
  }

  return maxLex;
}

// -----[ rel ]-----------------------------------------------------------------
// Description:     Translate relational symbol to Operation value for relation
//
// Returns:         Integer operation value for relation
int rel(int relOp) {

  switch (relOp) {
    case 8:
      return ODD;
    case 9:
      return EQL;
      break;
    case 10:
      return NEQ;
      break;
    case 11:
      return LSS;
      break;
    case 12:
      return LEQ;
      break;
    case 13:
      return GTR;
      break;
    case 14:
      return GEQ;
      break;
    default:
      break;
  }

  return -1;
}

// -----[ factor ]--------------------------------------------------------------
// factor ::= ident | number | '(' expression ')'
//
// Returns:         The current token
int factor(FILE* in, int token) {

  symbol* symbol;

  if(DEBUG) debug("FACTOR ENTERED", CYN);

  if(token == identsym) {
    {
      char buffer[50];
      fscanf(in, "%s", buffer);
      symbol = findSym(buffer);

      if(symbol->kind == variable) {
        emit(LOD, lexPtr - symbol->level, symbol->addr);
      } else if(symbol->kind == constant) {
        emit(LIT, 0, symbol->val);
      } else {
        error("Invalid type. Must be a Vaiable or constant.");
      }

      token = readToken(in);
    }
  } else if(token == numbersym) {
    {
      int value;
      fscanf(in, "%d", &value);
      emit(LIT, 0, value);

      token = readToken(in);
    }
  } else if(token == lparentsym) {
    token = readToken(in);
    token = expression(in, token);
    if(token != rparentsym) error("Right parenthesis missing");
    token = readToken(in);
  } else {
    error("Unexpected symbol encountered");
  }

  return token;
}

// -----[ term ]----------------------------------------------------------------
// term ::= factor {('*'|'/') factor}
//
// Returns:         The current token
int term(FILE* in, int token) {

  int multOp;

  if(DEBUG) debug("TERM ENTERED", CYN);

  token = factor(in, token);

  while(token == multsym || token == slashsym) {

    multOp = token;
    token = readToken(in);
    token = factor(in, token);

    if(multOp == multsym) emit(OPR, 0, 4);
    else if(multOp == slashsym) emit(OPR, 0, 5);
  }

  return token;
}

// -----[ expression ]----------------------------------------------------------
// expression ::= ['+' | '-'] term {('+' | '-') term}
//
// Returns:         The current token
int expression(FILE* in, int token) {

  int addOp;

  if(DEBUG) debug("EXPERSSION ENTERED", CYN);

  if(token == plussym || token == minussym) token = readToken(in);
  token = term(in, token);


  while(token == plussym || token == minussym) {

    addOp = token;
    token = readToken(in);
    token = term(in, token);

    if(addOp == plussym) emit(OPR, 0, 2);
    else if(addOp == minussym) emit(OPR, 0, 3);
  }

  return token;
}

// -----[ condition ]-----------------------------------------------------------
// condition ::= 'odd' experssion | expression rel-op expression
//
// Returns:         The current token
int condition(FILE* in, int token) {

  int relOp;

  if(DEBUG) debug("CONDITION ENTERED", CYN);

  if(token == oddsym) {
    token = readToken(in);
    token = expression(in, token);
    emit(OPR, 0, 6);
  } else {
    token = expression(in, token);
    // rel-op ::= '=' | '<>' | '<' | '<=' | '>' | '>='
    if(token <= oddsym && token >= geqsym)
      error("Relational operator expected");
    relOp = token;
    token = readToken(in);
    token = expression(in, token);
    emit(OPR, 0, rel(relOp));
  }

  return token;
}

// -----[ statement ]---------------------------------------------------------
// statement ::= [ ident ':=' expression
//    | 'call' ident
//    | 'begin' statement {';' statement} 'end'
//    | 'if' condition 'then' statement ['else' statement]
//    | 'while' condition 'do' statement
//    | 'read' ident
//    | 'write' ident
//    | e ]

// Returns:         The current token
int statement(FILE* in, int token) {

  int jmpAddr, jmpAddr2;

  if(DEBUG) debug("STATEMENT ENTERED", CYN);

  if(token == identsym) {
    // Check to see if identsym is defined in symbol_table
    char buffer[50];
    fscanf(in, "%s", buffer);
    symbol* symbol = findSym(buffer);
    if(symbol == NULL || symbol->level > lexPtr)
    error("Undeclared identifier");
    else if(symbol->kind != variable)
    error("Assignment to constant or procedure is not allowed");

    token = readToken(in);
    if(token != becomessym) error("Identifier must be followed by ':='");
    token = readToken(in);
    token = expression(in, token);

    emit(STO, lexPtr - symbol->level, symbol->addr);
  } else if(token == callsym) {
    token = readToken(in);
    if(token != identsym) error("call must be follwed by an identifier");

    // Check to see if identsym is defined in symbol_table
    char buffer[50];
    fscanf(in, "%s", buffer);
    symbol* symbol = findSym(buffer);
    if(symbol == NULL || symbol->level > lexPtr)
    error("Undeclared identifier");
    else if(symbol->kind != procedure)
    error("Calling a constant or variable is not allowed");

    emit(CAL, lexPtr - symbol->level, symbol->addr);
    // fscanf(in, "%*s");
    token = readToken(in);
  } else if(token == beginsym) {
    token = readToken(in);
    token = statement(in, token);

    while(token == semicolonsym) {
      token = readToken(in);
      token = statement(in, token);
    }

    if(token != endsym) error("'end' expected but not found");
    token = readToken(in);
  } else if(token == ifsym) {
    token = readToken(in);
    token = condition(in, token);
    if(token != thensym) error("'then' expected");
    token = readToken(in);
    if(token != beginsym) error("'begin' expected");
    jmpAddr = codePtr;
    emit(JPC, 0, 0);

    token = statement(in, token);

    if(token == elsesym) {
      token = readToken(in);
      code[jmpAddr].M = codePtr + 1;
      jmpAddr = codePtr;
      emit(JMP, 0, 0);
      token = statement(in, token);
      code[jmpAddr].M = codePtr;
    } else {
      emit(JMP, 0, codePtr+1);
      code[jmpAddr].M = codePtr;
    }
  } else if(token == whilesym) {
    jmpAddr = codePtr;
    token = readToken(in);
    token = condition(in, token);
    jmpAddr2 = codePtr;
    emit(JPC, 0, 0);
    if(token != dosym) error("'do' expected");
    token = readToken(in);
    token = statement(in, token);
    emit(JMP, 0, jmpAddr);
    code[jmpAddr2].M = codePtr;
  }

  return token;
}

// -----[ block ]-------------------------------------------------------------
// block ::= const-declaration var-declaration proc-delaration statement
//
// Returns:         The current token
int block(FILE* in, int token) {

  if(DEBUG) debug("BLOCK ENTERED", CYN);

  int jmpAddr,
  varCount = 0;

  lexPtr++;
  jmpAddr = codePtr;
  emit(JMP, 0, 0);

  // const-declaration ::= ['const' ident '=' number {','ident '=' number}';']
  if(token == constsym) {
    do {

      token = readToken(in);
      if(token != identsym)
      error("const, var, procedure must be followed by identifier");
      { // Constant: Kind, Name
        symbol_table[symPtr].kind = constant;
        symbol_table[symPtr].level = lexPtr;
        fscanf(in, "%s", symbol_table[symPtr].name);
      }
      // fscanf(in, "%*s");
      token = readToken(in);
      if(token != eqsym) error("'=' expected");
      token = readToken(in);
      if(token != numbersym)
      error("'=' must be followed by a number");
      { // Constant: Value => increment
        fscanf(in, "%d", &symbol_table[symPtr].val);

        if(DEBUG) {
          char buffer[100];
          sprintf(buffer,
            "    SYMBOL -> Type: Constant; Name: %s; Level: %d; Value: %d;",
            symbol_table[symPtr].name, symbol_table[symPtr].level,
            symbol_table[symPtr].val);
          debug(buffer, BLU);
        }

        symPtr++;
      }
      // fscanf(in, "%*d");
      token = readToken(in);
    } while(token == commasym);

    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);
  }

  // var-declaration ::= ['var' ident {',' ident} ';']
  if(token == varsym) {
    do {

      token = readToken(in);
      if(token != identsym)
      error("const, var, procedure must be followed by identifier");
      { // Variable: Kind, Name, L, and M => increment
        symbol_table[symPtr].kind = variable;
        fscanf(in, "%s", symbol_table[symPtr].name);
        symbol_table[symPtr].level = lexPtr;
        symbol_table[symPtr].addr = stackPtr++;

        if(DEBUG) {
          char buffer[100];
          sprintf(buffer,
            "    SYMBOL -> Type: Variable; Name: %s; Level: %d; Address: %d;",
            symbol_table[symPtr].name, symbol_table[symPtr].level,
            symbol_table[symPtr].addr);
          debug(buffer, BLU);
        }

        symPtr++;
        varCount++;
      }
      // fscanf(in, "%*s");
      token = readToken(in);
    } while(token == commasym);

    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);
  }

  // proc-declaration ::= {'procedure' ident ';' block ';'}
  while(token == procsym) {
    token = readToken(in);
    if(token != identsym)
    error("const, var, procedure must be followed by identifier");
    { // Procedure: Kind, Name, L, and M => increment

      // lexStackHist[lexPtr] = stackPtr;
      stackPtr = 4;

      symbol_table[symPtr].kind = procedure;
      fscanf(in, "%s", symbol_table[symPtr].name);
      symbol_table[symPtr].level = lexPtr;
      symbol_table[symPtr].addr = ++procCount;

      if(DEBUG) {
        char buffer[100];
        sprintf(buffer,
          "    SYMBOL -> Type: Procedure; Name: %s; Level: %d; Address: %d;",
          symbol_table[symPtr].name, symbol_table[symPtr].level,
          symbol_table[symPtr].addr);
        debug(buffer, BLU);
      }

      symPtr++;
    }
    // fscanf(in, "%*s");
    token = readToken(in);
    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);
    token = block(in, token);
    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);

    // stackPtr = lexStackHist[--lexPtr];
  }

  code[jmpAddr].M = codePtr;
  emit(INC, 0, varCount + 4);

  token = statement(in, token);

  if(lexPtr > 0) emit(OPR, 0, 0);
  lexPtr--;

  return token;
}

// -----[ program ]-------------------------------------------------------------
// Description:     Sets up code to run on automaton and begins the automaton.
//
// program ::= block '.'
void program(char* file) {

  if(DEBUG) debug("PROGRAM ENTERED", CYN);

  int token;

  symPtr = 0;
  codePtr = 0;
  lexPtr = -1;
  stackPtr = 4;
  procCount = 0;

  in = fopen(file, "r");
  if (in == NULL) error("Cannot open file");

  token = readToken(in);

  token = block(in, token);
  if(token != periodsym) {
    error("Period expected");
  }

  emit(SIO3, 0, 3);
}

// -----[ symbolPrint ]---------------------------------------------------------
// Description:     Write the symbol table to a file 'symboltable.txt'
void symbolPrint() {

  int i;

  FILE* out = fopen("symboltable.txt", "w");

  fprintf(out, "Name\tType\tLevel\tValue\n");

  for(i = 0; i < symPtr; i++) {
    fprintf(out, "%s\t", symbol_table[i].name);

    switch(symbol_table[i].kind) {
      case 1:
        fprintf(out, "const\t%d\t%d\n",
          symbol_table[i].level, symbol_table[i].val);
        break;
      case 2:
        fprintf(out, "var\t%d\t%d\n",
          symbol_table[i].level, symbol_table[i].addr);
        break;
      case 3:
        fprintf(out, "proc\t%d\t%d\n",
          symbol_table[i].level, symbol_table[i].addr);
        break;
      default:
        break;
    }
  }

  fclose(out);
}

// -----[ codePrint ]-----------------------------------------------------------
// Description:     Write the machine code to a file 'mcode.txt'
void codePrint() {

  int i;
  FILE* out = fopen("mcode.txt", "w");

  for(i = 0; i < codePtr; i++) {
    fprintf(out, "%d %d %d\n", code[i].OP, code[i].L, code[i].M);
  }

  fclose(out);
}

// *****************************************************************************

// -----[ main ]----------------------------------------------------------------
int main(int argc, char **argv) {

  if(!verifyArgs(argc, argv)) return 0;

  if(DEBUG) {
    char buffer[100];
    sprintf(buffer, "FILE: '%s'", argv[1]);
    debug(buffer, GRN);
  }

  program(argv[1]);
  symbolPrint();
  codePrint();

  return 0;
}
