## 4.1.0 (2015-11-25)

  - Flags implemented for -t, -s, -m, -a, -v

## 4.0.0 (2015-11-23)

  - compiler.c created to run all three programs as one.

  *TODO*
  - Clean up code.
  - Make it consistent

## 3.5.0 (2015-11-23)

  - Error messages updated
  - Submission files created

## 3.4.1 (2015-11-17)

  *Features*
  - run.sh shell script created to run all three programs on a sample input

## 3.4.0 (2015-11-15)

  *Bug Fixes*
  - Scanner separates ');' into two separate tokens now
  - Call is mapped to token 27 instead of token 2

## 3.3.3 (2015-11-15)

  - PARCER complete

  *Bug Fixes*
  - Store and load command fixed to load correct variable from symbol table.

## 3.3.2 (2015-11-15)

  *Bug Fixes*
  - Code generation no longer misses jump instruction at end of if block

## 3.3.1 (2015-11-15)

  *Bug Fixes*
  - Relational operation emit fixed
  - Extra '2 0 0' striped from all outputs

## 3.3.0 (2015-11-15)

  - Code generation written

  *Known Bugs*
  - Code generation misses one instruction causing jumps to be one address off
    *Issue may correspond to if statements*
  - Relational operations need the logic for L and M calculations
  - Relational operations displaying incorrect OP code
  - An extra '2 0 0' is generated in the code before SIO3 (hault)
  - Store and load commands tend to output wrong L and M

## 3.2.2 (2015-11-12)

  - Debug updated

## 3.2.1 (2015-11-12)

  *Bugfixes*
  - Undefined variable error detected
  - Assignment to a constant or procedure error detected

## 3.2.0 (2015-11-12)

  - symboltable.txt generation completed (Not extensibly tested)

## 3.1.0 (2015-11-9)

  - parser.c added
  - checks for errors in code syntax

  *Known Bugs*
  - not all of the errors in the sample cases are caught

## 2.0.4 (2015-10-19)

  - Added check for \t to prevent invalid token error

## 2.0.3 (2015-10-18)

  - Merged changes between Mario, DQ, and Austin.
  - Added \r checker

## 2.0.2 (2015-10-18)
  - Added flag in otherToken() function.
    *Sets the token value of any long series of spaces to -1.*
  - Added test cases.

    *Notes*
  - Added function descriptions to otherToken(), alphaToken().
  - Tested program on Eustis and is cleared for submission.
  - Test included the following:
                                *Variables and literals longer then max values.*
                                *Handling of heavily commented code.*
                                *Identification of various reserved words.*
                                *Identification of comparsions and arithemtic operators.*
                                *All printing scenarios.*

## 2.0.1 (2015-10-18)

  *Features*
  - Adds error checking for digit length           -- see 'digitToken()'
  - Adds error checking for identifier length      -- see 'alphaToken()'
  - Adds error function to handle the above errors -- see 'errorFound()'
    - 1) Prints to 'stdout'; Console by default. May need to be coloured
    - 2) Does a clean exit
  - Adds a global 'lineCount' variable for error messages

  *Notes*
  * Comments added for others to review changes -- search "(Mario 10/18)"
    - ^Small changes, such as '+1' on some memory allocations
  * Changes have been tested locally on Windows command line; ready for review.

## 2.0.0 (2015-10-18)

  *Features*
  - Removes comments and creates a new input file
  - Scans through code and creates lexeme table and token list

## 1.0.2 (2015-10-17)

  *Features*
  - verifyArgs Added
  - File opened

## 1.0.1 (2015-10-17)

  - Add Scanner.c

## 1.0.0 (2015-09-25)

  *Features*
  - Read in command line arguments to specify input and output file names
  - Save instructions as hex values (Ex. 0x700A OP:7 L:0 M:10)
  - Operations Added
    - LIT 0 M Push the literal value M onto the stack.
    - OPR 0 0 Return from a procedure call.
    - OPR 0 M Perform an ALU operation, specified by M.
    - LOD L M Read the value at offset M from L levels down (if L=0, our own frame) and push it onto the stack.
    - STO L M Pop the stack and write the value into offset M from L levels down if L=0, our own frame.
    - CAL L M Call the procedure at M.
    - INC 0 M Allocate enough space for M local variables.  We will always allocate at least four.
    - JMP 0 M Branch to M.
    - JPC 0 M Pop the stack and branch to M if the result is 0.
    - SIO 0 1 Pop the stack and write the result to the screen.
    - SIO 0 2 Read an input from the user and store it at the top of the stack.
    - SIO 0 3 Stop the machine.
  - ALU Operations Added
    - 1 NEG Pop the stack and push the negation of the result.
    - 2 ADD Pop the stack twice, add the values, and push the result.
    - 3 SUB Pop the stack twice, subtract the top value from the second value, and push the result.
    - 4 MUL Pop the stack twice, multiply the values, and push the result.
    - 5 DIV Pop the stack twice, divide the second value by the top value, and push the quotient.
    - 6 ODD Pop the stack, push 1 if the value is odd, and push 0 otherwise.
    - 7 MOD Pop the stack twice, divide the second value by the top value, and push the remainder.
    - 8 EQL Pop the stack twice and compare the top value t with the second value s.  Push 1 if s = t and 0 otherwise.
    - 9 NEQ Pop the stack twice and compare the top value t with the second value s.  Push 1 if s ≠ t and 0 otherwise.
    - 10 LSS Pop the stack twice and compare the top value t with the second value s.  Push 1 if s < t and 0 otherwise.
    - 11 LEQ Pop the stack twice and compare the top value t with the second value s.  Push 1 if s ≤ t and 0 otherwise.
    - 12 GTR Pop the stack twice and compare the top value t with the second value s.  Push 1 if s > t and 0 otherwise.
    - 13 GEQ Pop the stack twice and compare the top value t with the second value s.  Push 1 if s ≥ t and 0 otherwise.
