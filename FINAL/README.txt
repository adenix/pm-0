********************************************************************************
** Author: Austin Nicholas
**         DaQueshia Irvin
**         Mario Carrasco
**
** PM/0 COMPILER -- README.txt
********************************************************************************

### RUNNING PM/0 COMPILER

PM/0: To run compiler.c use the following syntax

   >   ./a.out [INPUT]
   *   [INPUT] - text file formated to contain the input.

   >   Flags
   *   -t	Print Token list
   *   -s	Print Symbol Table
   *   -m	Print Machine Code
   *   -a	Print Disassembled Code
   *   -v	Print Stack trace

### DEBUG MODE

At the top of the 'Defines' section in compiler.c there is the following line of code:
   15 // DEBUG
   16  #define DEBUG 0

Change the '0' to a '1' to run the program in Debug mode.
