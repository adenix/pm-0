/*******************************************************************************
 ** Author: Austin Nicholas
 **         DaQueshia Irvin
 **         Mario Carrasco
 **
 ** PM/0 Scanner -- compiler.c v4.1.0
 ******************************************************************************/

 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>
 #include <ctype.h>

 // -----[ Defines ]-------------------------------------------------------------
 // DEBUG
 #define DEBUG 0

 // dummy bool data type
 typedef int bool;
 #define true  1
 #define false 0

 #define MAX_IDENTIFIERS 11
 #define MAX_NUMBERS 5
 #define DIGIT_ERR 1 //(Mario 10/18)
 #define ALPHA_ERR 2
 #define INVALID_VAR_ERR 3
 #define INVALID_SYM_ERR 4
 #define MAX_SYMBOL_TABLE_SIZE 100
 #define MAX_CODE_LENGTH 500
 #define MAX_STACK_HEIGHT 2000
 #define MAX_LEXI_LEVELS 3

 // Command line color codes
 #define NRM  "\x1B[0m"
 #define RED  "\x1B[31m"
 #define GRN  "\x1B[32m"
 #define YEL  "\x1B[33m"
 #define BLU  "\x1B[34m"
 #define MAG  "\x1B[35m"
 #define CYN  "\x1B[36m"
 #define WHT  "\x1B[37m"

 // -----[ Structs ]------------------------------------------------------------
 typedef struct {
   char* name;
   int value;
 } token;

 // For constants, store kind, name, and value
 // For variables, store kind, name, L, and M
 // For procedures, store kind, name, L, and M
 typedef struct {
   int kind;       // const = 1, var = 2, proc = 3
   char name[12];  // name up to 11 characters
   int val;        // value
   int level;      // L level
   int addr;       // M address
 } symbol;

 typedef struct {
   int OP;     // Op code
   int L;      // Lex level
   int M;      // Address
 } instruction;

 typedef struct {
   bool t;
   bool s;
   bool m;
   bool a;
   bool v;
   char* file;
 } arguments;

 // -----[ Enumerators ]--------------------------------------------------------
 typedef enum {
   nulsym = 1, identsym, numbersym, plussym, minussym, multsym,  slashsym,
   oddsym, eqsym, neqsym, lessym, leqsym, gtrsym, geqsym, lparentsym,
   rparentsym, commasym, semicolonsym, periodsym, becomessym, beginsym,
   endsym, ifsym, thensym, whilesym, dosym, callsym, constsym, varsym, procsym,
   writesym, readsym, elsesym
 } token_type;

 typedef enum {
   constant = 1, variable, procedure
 } symbol_type;

 typedef enum {
   LIT = 1, OPR, LOD, STO, CAL, INC, JMP, JPC, SIO1, SIO2, SIO3
 } op_code;

 typedef enum {
   NEG = 1, ADD, SUB, MUL, DIV, ODD, MOD, EQL, NEQ, LSS, LEQ, GTR, GEQ
 } relation;

 // -----[ Global Variables ]----------------------------------------------------
 int lineCount = 1; //For printing error messages (Mario 10/18)

 FILE* in;
 FILE* cleaninput;
 FILE* lexemetable;
 FILE* tokenlist;
  FILE *fileOut;

 int symPtr,
     codePtr,
     lexPtr,
     stackPtr;

 int procCount;


 symbol symbol_table[MAX_SYMBOL_TABLE_SIZE];
 instruction code[MAX_CODE_LENGTH];
 // int lexStackHist[MAX_LEXI_LEVELS];

 // Operation code Mnemonic array. Provides Order(1) look up for operation names
 const char* MNEMONIC[11] = {"lit", "opr", "lod", "sto", "cal", "inc", "jmp",
   "jpc", "sio", "sio", "sio"};

 // Max Values for PM/0 Machine
 // const int MAX_STACK_HEIGHT = 2000;
 // const int MAX_CODE_LENGTH = 500;
 // const int MAX_LEXI_LEVELS = 3;

 // Stack Declaration in array form
 int stack[MAX_STACK_HEIGHT];
 // Used to track where lex levels start. Allows ' | ' to be printed
 int lex[3];

 // Memory array to hold OP L, M in hex form
 unsigned memory[MAX_CODE_LENGTH];
 unsigned IR = 0;    // Instruction Register

 int SP = 0,   // Stack Pointer
     BP = 1,   // Base Pointer
     PC = 0,   // Program Counter
     LC = 0;   // Lexicographical Counter

// -----[ Function Declaration (AS NEEDED) ]------------------------------------
int expression(FILE* in, int token);

// *****[ SCANNER ]*************************************************************

// -----[ errorFound ]----------------------------------------------------------
// Description:     Produces an error message and halts.
//
// Return:          Void.

void errorFound(int err)
{
    switch(err)
    {

        case DIGIT_ERR:
        printf(
          ">> ERROR -- Line %d: Max digit length has been exceeded.\n\n",
          lineCount);
        break;

        case ALPHA_ERR:
        printf(
          ">> ERROR -- Line %d: Max identifier length has been exceeded.\n\n",
          lineCount);
        break;

        case INVALID_VAR_ERR:
        printf(
          ">> ERROR -- Line %d: Invalid identifier.\n\n", lineCount);
        break;

        case INVALID_SYM_ERR:
        printf(
          ">> ERROR -- Line %d: Invalid symbol.\n\n", lineCount);
        break;

        default:
        printf(
          ">> ERROR -- Line %d: Unknown error.\n\n", lineCount);

    }

    exit(EXIT_FAILURE);

}

// -----[ alphaToken ]----------------------------------------------------------
// Description:     Reads in a series of characters, and creates a token for the
//                  symbol.
//                  * Produces an error and exits program if identifier length
//                    is exceeded.
//
// Return:          A token with either a literal or identifer with the
//                  appropiate value.
token alphaToken(FILE* in, char currentChar) {

    token temp;
    temp.name = (char*)calloc(MAX_IDENTIFIERS+1, sizeof(char));
    //                        ^ '+1' for the null char - (Mario 10/18)

    int i = 0;

    while(currentChar != EOF && (isalpha(currentChar) ||
      isdigit(currentChar))) {

        temp.name[i++] = currentChar;
        currentChar = fgetc(in);

        //Error --Max identifier length exceeded. (Mario 10/18)
        if(i > MAX_IDENTIFIERS) errorFound(ALPHA_ERR);
    }
    temp.name[i] = '\0';

    if(currentChar != EOF) fseek(in, -1, SEEK_CUR);

    if(!strcmp(temp.name, "odd"))
        temp.value = oddsym;
    if(!strcmp(temp.name, "begin"))
        temp.value = beginsym;
    else if(!strcmp(temp.name, "end"))
        temp.value = endsym;
    else if(!strcmp(temp.name, "if"))
        temp.value = ifsym;
    else if(!strcmp(temp.name, "then"))
        temp.value = thensym;
    else if(!strcmp(temp.name, "while"))
        temp.value = whilesym;
    else if(!strcmp(temp.name, "do"))
        temp.value = dosym;
    else if(!strcmp(temp.name, "call"))
        temp.value = callsym;
    else if(!strcmp(temp.name, "const"))
        temp.value = constsym;
    else if(!strcmp(temp.name, "var"))
        temp.value = varsym;
    else if(!strcmp(temp.name, "procedure"))
        temp.value = procsym;
    else if(!strcmp(temp.name, "write"))
        temp.value = writesym;
    else if(!strcmp(temp.name, "read"))
        temp.value = readsym;
    else if(!strcmp(temp.name, "else"))
        temp.value = elsesym;
    else
        temp.value = identsym;

    if(DEBUG) printf("  > Alpha Token: '%s' %d\n", temp.name, temp.value);

    return temp;
}

// -----[ digitToken ]----------------------------------------------------------
// Description:     Reads in a digit, and creates a token for the symbol.
//                  * Produces an error and exits program if digit length is
//                    exceeded.
//
// Return:          Token with the digit (char *) and numbersym value.
token digitToken(FILE* in, char currentChar) {

    token temp;
    temp.name = (char*)calloc(MAX_NUMBERS+1, sizeof(char));
    //                        ^ '+1' for the null char (Mario 10/18)

    int i = 0;

    while(currentChar != EOF && isdigit(currentChar)) {

        temp.name[i++] = currentChar;
        currentChar = fgetc(in);

        //Error --max digit length exceeded. (Mario 10/18)
        if(i > MAX_NUMBERS) errorFound(DIGIT_ERR);
    }
    //Error -- Invalid variable name.
    if(isalpha(currentChar))
        errorFound(INVALID_VAR_ERR);

    temp.name[i] = '\0';

    if(currentChar != EOF) fseek(in, -1, SEEK_CUR);

    temp.value = numbersym;

    if(DEBUG) printf("  > Digit Token: '%s' %d\n", temp.name, temp.value);

    return temp;
}

// -----[ otherToken ]----------------------------------------------------------
// Description:     A function for identifying arithmetic operators, comparison
//                  operators, and comments. Reads in currentChar and produces a
//                  token.
//
// Return:          Token with the symbol and appropriate value.
token otherToken(FILE* in, char currentChar) {

    bool firstChar = true;
    token temp;
    temp.name = (char*)calloc(5, sizeof(char));

    int i = 0;

    while(currentChar != EOF && (!isalpha(currentChar)) &&
      (!isdigit(currentChar)) && currentChar != ' ' && currentChar != '\n') {

        if(currentChar == ';') {
          if(!firstChar) break;
        }

        temp.name[i++] = currentChar;
        currentChar = fgetc(in);
        firstChar = false;
    }
    temp.name[i] = '\0';

    for(i = 0; i < strlen(temp.name); i++) {
        if(temp.name[i] == '\n')
            temp.name[i] = '\0';
    }

    if(currentChar != EOF) fseek(in, -1, SEEK_CUR);

    if(!strcmp(temp.name, " "))
        temp.value = nulsym;
    else if(!strcmp(temp.name, "+"))
        temp.value = plussym;
    else if(!strcmp(temp.name, "-"))
        temp.value = minussym;
    else if(!strcmp(temp.name, "*"))
        temp.value = multsym;
    else if(!strcmp(temp.name, "/"))
        temp.value = slashsym;
    else if(!strcmp(temp.name, "="))
        temp.value = eqsym;
    else if(!strcmp(temp.name, "<>"))
        temp.value = neqsym;
    else if(!strcmp(temp.name, "<"))
        temp.value = lessym;
    else if(!strcmp(temp.name, "<="))
        temp.value = leqsym;
    else if(!strcmp(temp.name, ">"))
        temp.value = gtrsym;
    else if(!strcmp(temp.name, ">="))
        temp.value = geqsym;
    else if(!strcmp(temp.name, "("))
        temp.value = lparentsym;
    else if(!strcmp(temp.name, ")"))
        temp.value = rparentsym;
    else if(!strcmp(temp.name, ","))
        temp.value = commasym;
    else if(!strcmp(temp.name, ";"))
        temp.value = semicolonsym;
    else if(!strcmp(temp.name, "."))
        temp.value = periodsym;
    else if(!strcmp(temp.name, ":="))
        temp.value = becomessym;
    else{
        if(strcmp(temp.name," ")) // An invalid Symbol
            errorFound(INVALID_SYM_ERR);
        else
        temp.value = -1; // set flag for a series of blank spaces.
    }


    if(DEBUG) printf("  > Other Token: '%s' %d\n", temp.name, temp.value);

    return temp;
}

int scannerMain(char* file) {
  char currentChar;

  // if(!verifyArgs(argc, argv)) return 0;

  // -----[ File Open ]---------------------------------
  in = fopen(file, "r");
  cleaninput = fopen("cleaninput.txt", "w");
  lexemetable = fopen("lexemetable.txt", "w");
  tokenlist = fopen("tokenlist.txt", "w");

  fprintf(lexemetable, "lexeme\ttoken type\n");

  // -----[ Body ]--------------------------------------
  if(DEBUG) printf(" > RUNNING: Clean Input\n");

  while((currentChar = fgetc(in)) != EOF) {

      if(currentChar == '/') {
          if((currentChar = fgetc(in)) == '*') {
            if(DEBUG) printf("  > Removing Comment: '/*");
              while(true) {
                  if((currentChar = fgetc(in)) == '*') {
                      if((currentChar = fgetc(in)) == '/')
                          break;
                      else
                        if(DEBUG) printf("*%c", currentChar);
                  } else {
                    if(DEBUG) printf("%c", currentChar);
                  }
              }
              if(DEBUG) printf("*/'\n");
          }
          else {
            fprintf(cleaninput, "/%c", currentChar);
          }

      } else if(currentChar == '\r') {
        if(DEBUG) printf("  > Removing \\r\n");
        continue;
      } else {
        fprintf(cleaninput, "%c", currentChar);
      }
  }

  fclose(in);
  fclose(cleaninput);
  cleaninput = fopen("cleaninput.txt", "r");

  if(DEBUG) printf(" > RUNNING: Scanner\n");
  while((currentChar = fgetc(cleaninput)) != EOF) {

      token token;

      if(currentChar == ' ' || currentChar == '\n' || currentChar == '\t'){

          //Increment lineCount for error-message printouts (Mario 10/18)
          if(currentChar == '\n')
              lineCount++;

          continue;
      }
      else if(isalpha(currentChar))
        token = alphaToken(cleaninput, currentChar);
      else if(isdigit(currentChar))
        token = digitToken(cleaninput, currentChar);
      else
        token = otherToken(in, currentChar);

      if(token.value != -1){ // Prevents printing of blank spaces
          fprintf(lexemetable, "%s\t%d\n", token.name, token.value);
      }
      if(token.value == 2 || token.value == 3)
          fprintf(tokenlist, "%d %s ", token.value, token.name);
      else if (token.value != -1)// Prevents printing of blank spaces
          fprintf(tokenlist, "%d ", token.value);
  }

  // -----[ File Close ]--------------------------------
  fclose(cleaninput);
  fclose(lexemetable);
  fclose(tokenlist);

  return 0;
}

// *****[ END SCANNER ]*********************************************************
// *****[ PARSER ]**************************************************************

void debug(char* msg, char* CLR) {
  printf("%s  DEBUG > %s%s\n", CLR, msg, NRM);
}

// -----[ error ]---------------------------------------------------------------
// Description:     Verbos error message to screen and exit(0)
void error(char* msg){
  printf("ERROR: %s\n", msg);
  exit(0);
}

// -----[ readToken ]-----------------------------------------------------------
// Description:     Reads in next token from 'tokenlist.txt' and displays a
//                  debug message if DEBUG flag is '1'
//
// Returns:         The token retrived from 'tokenlist.txt'
int readToken(FILE* in) {

  int token;

  fscanf(in, "%d", &token);
  if(DEBUG) {
    char buffer[25];
    sprintf(buffer, "  Token '%d' read", token);
    debug(buffer, YEL);
  }

  return token;
}

// -----[ emit ]----------------------------------------------------------------
// Description:     Output PL/0 code
// print the appropriate code to the code file
void emit(int OP, int L, int M)
{
  code[codePtr].OP = OP;
  // code[codePtr].R = R;
  code[codePtr].L = L;
  code[codePtr].M = M;

  if(DEBUG) {
    char buffer[50];
    sprintf(buffer, "    EMIT: %d %d %d",
    code[codePtr].OP, code[codePtr].L, code[codePtr].M);
    debug(buffer, MAG);
  }

  codePtr++;
}

// -----[ findSym ]-------------------------------------------------------------
// Description:     Search symbol table for a specific symbol.
//
// Returns:         A pointer to the symbol if found. Other wise NULL
symbol* findSym(char* name) {

  int i;
  symbol* maxLex = NULL;

  for(i = 0; i < symPtr; i++) {
    if(!strcmp(name, symbol_table[i].name) &&
      (symbol_table[i].level <= lexPtr)) maxLex = &symbol_table[i];
  }

  return maxLex;
}

// -----[ rel ]-----------------------------------------------------------------
// Description:     Translate relational symbol to Operation value for relation
//
// Returns:         Integer operation value for relation
int rel(int relOp) {

  switch (relOp) {
    case 8:
      return ODD;
    case 9:
      return EQL;
      break;
    case 10:
      return NEQ;
      break;
    case 11:
      return LSS;
      break;
    case 12:
      return LEQ;
      break;
    case 13:
      return GTR;
      break;
    case 14:
      return GEQ;
      break;
    default:
      break;
  }

  return -1;
}

// -----[ factor ]--------------------------------------------------------------
// factor ::= ident | number | '(' expression ')'
//
// Returns:         The current token
int factor(FILE* in, int token) {

  symbol* symbol;

  if(DEBUG) debug("FACTOR ENTERED", CYN);

  if(token == identsym) {
    {
      char buffer[50];
      fscanf(in, "%s", buffer);
      symbol = findSym(buffer);

      if(symbol->kind == variable) {
        emit(LOD, lexPtr - symbol->level, symbol->addr);
      } else if(symbol->kind == constant) {
        emit(LIT, 0, symbol->val);
      } else {
        error("Invalid type. Must be a Vaiable or constant.");
      }

      token = readToken(in);
    }
  } else if(token == numbersym) {
    {
      int value;
      fscanf(in, "%d", &value);
      emit(LIT, 0, value);

      token = readToken(in);
    }
  } else if(token == lparentsym) {
    token = readToken(in);
    token = expression(in, token);
    if(token != rparentsym) error("Right parenthesis missing");
    token = readToken(in);
  } else {
    error("Unexpected symbol encountered");
  }

  return token;
}

// -----[ term ]----------------------------------------------------------------
// term ::= factor {('*'|'/') factor}
//
// Returns:         The current token
int term(FILE* in, int token) {

  int multOp;

  if(DEBUG) debug("TERM ENTERED", CYN);

  token = factor(in, token);

  while(token == multsym || token == slashsym) {

    multOp = token;
    token = readToken(in);
    token = factor(in, token);

    if(multOp == multsym) emit(OPR, 0, 4);
    else if(multOp == slashsym) emit(OPR, 0, 5);
  }

  return token;
}

// -----[ expression ]----------------------------------------------------------
// expression ::= ['+' | '-'] term {('+' | '-') term}
//
// Returns:         The current token
int expression(FILE* in, int token) {

  int addOp;

  if(DEBUG) debug("EXPERSSION ENTERED", CYN);

  if(token == plussym || token == minussym) token = readToken(in);
  token = term(in, token);


  while(token == plussym || token == minussym) {

    addOp = token;
    token = readToken(in);
    token = term(in, token);

    if(addOp == plussym) emit(OPR, 0, 2);
    else if(addOp == minussym) emit(OPR, 0, 3);
  }

  return token;
}

// -----[ condition ]-----------------------------------------------------------
// condition ::= 'odd' experssion | expression rel-op expression
//
// Returns:         The current token
int condition(FILE* in, int token) {

  int relOp;

  if(DEBUG) debug("CONDITION ENTERED", CYN);

  if(token == oddsym) {
    token = readToken(in);
    token = expression(in, token);
    emit(OPR, 0, 6);
  } else {
    token = expression(in, token);
    // rel-op ::= '=' | '<>' | '<' | '<=' | '>' | '>='
    if(token <= oddsym && token >= geqsym)
      error("Relational operator expected");
    relOp = token;
    token = readToken(in);
    token = expression(in, token);
    emit(OPR, 0, rel(relOp));
  }

  return token;
}

// -----[ statement ]---------------------------------------------------------
// statement ::= [ ident ':=' expression
//    | 'call' ident
//    | 'begin' statement {';' statement} 'end'
//    | 'if' condition 'then' statement ['else' statement]
//    | 'while' condition 'do' statement
//    | 'read' ident
//    | 'write' ident
//    | e ]

// Returns:         The current token
int statement(FILE* in, int token) {

  int jmpAddr, jmpAddr2;

  if(DEBUG) debug("STATEMENT ENTERED", CYN);

  if(token == identsym) {
    // Check to see if identsym is defined in symbol_table
    char buffer[50];
    fscanf(in, "%s", buffer);
    symbol* symbol = findSym(buffer);
    if(symbol == NULL || symbol->level > lexPtr)
    error("Undeclared identifier");
    else if(symbol->kind != variable)
    error("Assignment to constant or procedure is not allowed");

    token = readToken(in);
    if(token != becomessym) error("Identifier must be followed by ':='");
    token = readToken(in);
    token = expression(in, token);

    emit(STO, lexPtr - symbol->level, symbol->addr);
  } else if(token == callsym) {
    token = readToken(in);
    if(token != identsym) error("call must be follwed by an identifier");

    // Check to see if identsym is defined in symbol_table
    char buffer[50];
    fscanf(in, "%s", buffer);
    symbol* symbol = findSym(buffer);
    if(symbol == NULL || symbol->level > lexPtr)
    error("Undeclared identifier");
    else if(symbol->kind != procedure)
    error("Calling a constant or variable is not allowed");

    emit(CAL, lexPtr - symbol->level, symbol->addr);
    // fscanf(in, "%*s");
    token = readToken(in);
  } else if(token == beginsym) {
    token = readToken(in);
    token = statement(in, token);

    while(token == semicolonsym) {
      token = readToken(in);
      token = statement(in, token);
    }

    if(token != endsym) error("'end' expected but not found");
    token = readToken(in);
  } else if(token == ifsym) {
    token = readToken(in);
    token = condition(in, token);
    if(token != thensym) error("'then' expected");
    token = readToken(in);
    if(token != beginsym) error("'begin' expected");
    jmpAddr = codePtr;
    emit(JPC, 0, 0);

    token = statement(in, token);

    if(token == elsesym) {
      token = readToken(in);
      code[jmpAddr].M = codePtr + 1;
      jmpAddr = codePtr;
      emit(JMP, 0, 0);
      token = statement(in, token);
      code[jmpAddr].M = codePtr;
    } else {
      emit(JMP, 0, codePtr+1);
      code[jmpAddr].M = codePtr;
    }
  } else if(token == whilesym) {
    jmpAddr = codePtr;
    token = readToken(in);
    token = condition(in, token);
    jmpAddr2 = codePtr;
    emit(JPC, 0, 0);
    if(token != dosym) error("'do' expected");
    token = readToken(in);
    token = statement(in, token);
    emit(JMP, 0, jmpAddr);
    code[jmpAddr2].M = codePtr;
  }

  return token;
}

// -----[ block ]-------------------------------------------------------------
// block ::= const-declaration var-declaration proc-delaration statement
//
// Returns:         The current token
int block(FILE* in, int token) {

  if(DEBUG) debug("BLOCK ENTERED", CYN);

  int jmpAddr,
  varCount = 0;

  lexPtr++;
  jmpAddr = codePtr;
  emit(JMP, 0, 0);

  // const-declaration ::= ['const' ident '=' number {','ident '=' number}';']
  if(token == constsym) {
    do {

      token = readToken(in);
      if(token != identsym)
      error("const, var, procedure must be followed by identifier");
      { // Constant: Kind, Name
        symbol_table[symPtr].kind = constant;
        symbol_table[symPtr].level = lexPtr;
        fscanf(in, "%s", symbol_table[symPtr].name);
      }
      // fscanf(in, "%*s");
      token = readToken(in);
      if(token != eqsym) error("'=' expected");
      token = readToken(in);
      if(token != numbersym)
      error("'=' must be followed by a number");
      { // Constant: Value => increment
        fscanf(in, "%d", &symbol_table[symPtr].val);

        if(DEBUG) {
          char buffer[100];
          sprintf(buffer,
            "    SYMBOL -> Type: Constant; Name: %s; Level: %d; Value: %d;",
            symbol_table[symPtr].name, symbol_table[symPtr].level,
            symbol_table[symPtr].val);
          debug(buffer, BLU);
        }

        symPtr++;
      }
      // fscanf(in, "%*d");
      token = readToken(in);
    } while(token == commasym);

    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);
  }

  // var-declaration ::= ['var' ident {',' ident} ';']
  if(token == varsym) {
    do {

      token = readToken(in);
      if(token != identsym)
      error("const, var, procedure must be followed by identifier");
      { // Variable: Kind, Name, L, and M => increment
        symbol_table[symPtr].kind = variable;
        fscanf(in, "%s", symbol_table[symPtr].name);
        symbol_table[symPtr].level = lexPtr;
        symbol_table[symPtr].addr = stackPtr++;

        if(DEBUG) {
          char buffer[100];
          sprintf(buffer,
            "    SYMBOL -> Type: Variable; Name: %s; Level: %d; Address: %d;",
            symbol_table[symPtr].name, symbol_table[symPtr].level,
            symbol_table[symPtr].addr);
          debug(buffer, BLU);
        }

        symPtr++;
        varCount++;
      }
      // fscanf(in, "%*s");
      token = readToken(in);
    } while(token == commasym);

    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);
  }

  // proc-declaration ::= {'procedure' ident ';' block ';'}
  while(token == procsym) {
    token = readToken(in);
    if(token != identsym)
    error("const, var, procedure must be followed by identifier");
    { // Procedure: Kind, Name, L, and M => increment

      // lexStackHist[lexPtr] = stackPtr;
      stackPtr = 4;

      symbol_table[symPtr].kind = procedure;
      fscanf(in, "%s", symbol_table[symPtr].name);
      symbol_table[symPtr].level = lexPtr;
      symbol_table[symPtr].addr = ++procCount;

      if(DEBUG) {
        char buffer[100];
        sprintf(buffer,
          "    SYMBOL -> Type: Procedure; Name: %s; Level: %d; Address: %d;",
          symbol_table[symPtr].name, symbol_table[symPtr].level,
          symbol_table[symPtr].addr);
        debug(buffer, BLU);
      }

      symPtr++;
    }
    // fscanf(in, "%*s");
    token = readToken(in);
    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);
    token = block(in, token);
    if(token != semicolonsym) error("Semicolon or comma missing");
    token = readToken(in);

    // stackPtr = lexStackHist[--lexPtr];
  }

  code[jmpAddr].M = codePtr;
  emit(INC, 0, varCount + 4);

  token = statement(in, token);

  if(lexPtr > 0) emit(OPR, 0, 0);
  lexPtr--;

  return token;
}

// -----[ program ]-------------------------------------------------------------
// Description:     Sets up code to run on automaton and begins the automaton.
//
// program ::= block '.'
void program(char* file) {

  if(DEBUG) debug("PROGRAM ENTERED", CYN);

  int token;

  symPtr = 0;
  codePtr = 0;
  lexPtr = -1;
  stackPtr = 4;
  procCount = 0;

  in = fopen(file, "r");
  if (in == NULL) error("Cannot open file");

  token = readToken(in);

  token = block(in, token);
  if(token != periodsym) {
    error("Period expected");
  }

  emit(SIO3, 0, 3);
}

// -----[ symbolPrint ]---------------------------------------------------------
// Description:     Write the symbol table to a file 'symboltable.txt'
void symbolPrint() {

  int i;

  FILE* out = fopen("symboltable.txt", "w");

  fprintf(out, "Name\tType\tLevel\tValue\n");

  for(i = 0; i < symPtr; i++) {
    fprintf(out, "%s\t", symbol_table[i].name);

    switch(symbol_table[i].kind) {
      case 1:
        fprintf(out, "const\t%d\t%d\n",
          symbol_table[i].level, symbol_table[i].val);
        break;
      case 2:
        fprintf(out, "var\t%d\t%d\n",
          symbol_table[i].level, symbol_table[i].addr);
        break;
      case 3:
        fprintf(out, "proc\t%d\t%d\n",
          symbol_table[i].level, symbol_table[i].addr);
        break;
      default:
        break;
    }
  }

  fclose(out);
}

// -----[ codePrint ]-----------------------------------------------------------
// Description:     Write the machine code to a file 'mcode.txt'
void codePrint() {

  int i;
  FILE* out = fopen("mcode.txt", "w");

  for(i = 0; i < codePtr; i++) {
    fprintf(out, "%d %d %d\n", code[i].OP, code[i].L, code[i].M);
  }

  fclose(out);
}

int parserMain() {

  program("tokenlist.txt");
  symbolPrint();
  codePrint();

  return 0;
}

// *****[ END PARSER ]**********************************************************
// *****[ VM ]******************************************************************

// -----[ ALU ]-----------------------------------------------------------------
// Description:     The Arithmentic Logic Unit, or ALU, handles all of the
//                  arithmetic that a program may utilize.
//
void ALU(unsigned M) {

  switch((int) M) {

    // NEG: Pop the stack and push the negation of the result.
    case 1:
      stack[SP] = -stack[SP];
      break;

    // ADD: Pop the stack twice, add the values, and push the result.
    case 2:
      stack[SP-1] = stack[SP] + stack[SP-1];
      SP--;
      break;

    // SUB: Pop the stack twice, subtract the top value from the second value,
    //      and push the result.
    case 3:
      stack[SP-1] = stack[SP-1] - stack[SP];
      SP--;
      break;

    // MUL: Pop the stack twice, multiply the values, and push the result.
    case 4:
      stack[SP-1] = stack[SP] * stack[SP-1];
      SP--;
      break;

    // DIV: Pop the stack twice, divide the second value by the top value, and
    //      push the quotient.
    case 5:
      stack[SP-1] = stack[SP-1] / stack[SP];
      SP--;
      break;

    // ODD: Pop the stack, push 1 if the value is odd, and push 0 otherwise.
    case 6:
      stack[SP] = (stack[SP] % 2) ? 0 : 1;
      break;

    // MOD: Pop the stack twice, divide the second value by the top value, and
    //      push the remainder.
    case 7:
      stack[SP-1] = stack[SP-1] % stack[SP];
      SP--;
      break;

    // EQL: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s = t and 0 otherwise.
    case 8:
      stack[SP-1] = (stack[SP] == stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // NEQ: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s ≠ t and 0 otherwise.
    case 9:
      stack[SP-1] = (stack[SP] != stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // LSS: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s < t and 0 otherwise.
    case 10:
      stack[SP-1] = (stack[SP] > stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // LEQ: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s ≤ t and 0 otherwise.
    case 11:
      stack[SP-1] = (stack[SP] >= stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // GTR: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s > t and 0 otherwise.
    case 12:
      stack[SP-1] = (stack[SP] < stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // GEQ: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s ≥ t and 0 otherwise.
    case 13:
      stack[SP-1] = (stack[SP] < stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // Invalid ALU Operation.
    default:
      break;
  }
}

// -----[ base ]----------------------------------------------------------------
// Description:     Given by professor
//
// Returns:         ...
//
int base(l) {

  int b1 = BP; // find base L levels down
  while (l > 0) {
    b1 = stack[b1 + 1];
    l--;
  }
  return b1;
}

// -----[ stackVerbos ]---------------------------------------------------------
// Description:     Print out the stack in a line.
//
void stackVerbos() {

  int i,
      j;
  // Print the stack to the file provided.
  for(i = 1; i <= SP; i++) {

    // Check to Lexicographical Levels and denote them with a '|'
    for(j = 0; j < LC; j++) {
      if(lex[j] == (i-1)) fprintf(fileOut, " |");
    }
    fprintf(fileOut, "%s%d", (i == 1) ? "\t" : " ", stack[i]);
  }
}

// -----[ programImport ]-------------------------------------------------------
// Description:     Import program from text file to MEMORY.
//
// Returns:         True if successful. False if not.
//
bool programImport(char *in){

  FILE *fileIn = fopen(in, "r");

  unsigned  OP,   // Operation Code
            L,    // Lexicographical Level
            M;    // Parameter

  int i;

  while(fscanf(fileIn, "%u", &OP) != EOF) {

    if(fscanf(fileIn, "%u", &L) == EOF) return false;
    if(fscanf(fileIn, "%u", &M) == EOF) return false;

    memory[PC++] = (OP << 12) | (L << 8) | M;
  }

  if (DEBUG) {
    printf("\n");

    printf("%s DEBUG: %sMEMORY\n", YEL, WHT);
    printf("%s   >   |-------------------------------------|%s\n", GRN, NRM);
    printf("%s   >   | MEM ADD |  HEX   |  OP  |  L  |  M  |\n%s", GRN, NRM);
    printf("%s   >   |-------------------------------------|%s\n", GRN, NRM);
    for(i = 0; i < PC; i++)
      printf("%s   >   |   [%2d]  | 0x%.4X |  %2d  |  %d  |  %2d |\n%s", GRN,
        i, memory[i], (memory[i] >> 12), (memory[i] >> 8) & 0x0F,
        memory[i] & 0x00FF, NRM);
    printf("%s   >   |-------------------------------------|%s\n", GRN, NRM);

    printf("\n");
  }

  fclose(fileIn);
  return true;
}

int vmMain() {
  int i;

  if(!programImport("mcode.txt")) return 0;

  // Open output file to be written to
  fileOut = fopen("stacktrace.txt", "w");

  // Write program header
  fprintf(fileOut, "Line\tOP\tL\tM\n");

  // Write program code
  for(i = 0; i < PC; i++) {
    fprintf(fileOut, "%2d\t%s\t%d\t%d\n", i, MNEMONIC[(memory[i] >> 12) - 1],
      (memory[i] >> 8) & 0x0F, memory[i] & 0x00FF);
  }

  PC = 0;

  // Write trace header
  fprintf(fileOut, "\n\t\t\t\tpc\tbp\tsp\tstack\n");
  fprintf(fileOut, "Initial values\t\t\t%d\t%d\t%d\n", PC, BP, SP);

  // Run program untill SIO 0, 3 is encountered AKA 0xB003
  while((IR = memory[PC++]) != 0xB003) {

    fprintf(fileOut, "%2d\t%s\t%d\t%d", (PC-1), MNEMONIC[(IR >> 12) - 1],
      (IR >> 8) & 0x0F, IR & 0x00FF);

    switch(IR >> 12) {

      // LIT 0, M:  Push the literal value M onto the stack.
      case 1:
        stack[++SP] = IR & 0x00FF;
        break;

      // OPR 0, 0:  Return from a procedure call.
      // OPR 0, M:  Perform an ALU operation, specified by M.
      case 2:
        if((IR & 0x00FF) == 0) {
          SP = BP-1;
          PC = stack[SP + 4];
          BP = stack[SP + 3];
          LC--;
        } else {
          ALU(IR & 0x00FF);
        }
        break;

      // LOD L, M:  Read the value at offset M from L levels down (if L=0, our
      //            own frame) and push it onto the stack.
      case 3:
        stack[++SP] = stack[base((IR >> 8) & 0x0F) + (IR & 0x00FF)];
        break;

      // STO L, M:  Pop the stack and write the value into offset M from L
      //            levels down – if L=0, our own frame.
      case 4:
        stack[base((IR >> 8) & 0x0F) + (IR & 0x00FF)] = stack[SP--];
        break;

      // CAL L, M:  Call the procedure at M.
      case 5:
        lex[LC++] = SP;
        stack[SP+1] = 0;
        stack[SP+2] = base((IR >> 8) & 0x0F);
        stack[SP+3] = BP;
        stack[SP+4] = PC;
        BP = SP + 1;
        PC = IR & 0x00FF;
        break;

      // INC 0, M:  Allocate enough space for M local variables.  We will always
      //            allocate at least four.
      case 6:
        SP = SP + (IR & 0x00FF);
        break;

      // JMP 0, M:  Branch to M.
      case 7:
        PC = (IR & 0x00FF);
        break;

      // JPC 0, M:  Pop the stack and branch to M if the result is 0.
      case 8:
        if (stack[SP--] == 0) {
          PC = (IR & 0x00FF);
        }
        break;

      // SIO 0, 1:  Pop the stack and write the result to the screen.
      case 9:
        printf("%d\n", stack[SP--]);
        break;

      // SIO 0, 2:  Read an input from the user and store it at the top of the
      //            stack.
      case 10:
        scanf("%d", &stack[SP++]);
        break;

      //
      default:
        break;
    }

    // Write PC BP and SP values
    fprintf(fileOut, "\t%d\t%d\t%d", PC, BP, SP);
    // Write stack
    stackVerbos();
    fprintf(fileOut, "\n");
  }

  fprintf(fileOut, "%2d\t%s\t%d\t%d", (PC-1), MNEMONIC[(IR >> 12) - 1],
    (IR >> 8) & 0x0F, IR & 0x00FF);
  fprintf(fileOut, "\n");

  fclose(fileOut);
  return 0;
}

// *****[ END VM ]**************************************************************

// -----[ args ]----------------------------------------------------------------
arguments args(int argc, char **argv) {

  int i;

  arguments arguments;
  arguments.file = NULL;
  arguments.t = arguments.s = arguments.m = arguments.a = arguments.v = false;

  if(!strcmp(argv[1], "--help")) {
    printf("\n");
    printf("%sPM/0: %sTo run compiler.c use the following syntax\n\n",YEL, NRM);
    printf("   >%s   ./a.out [INPUT]\n%s", GRN, NRM);
    printf("   *   [INPUT] - text file formated to contain the input.\n\n");
    printf("   >%s   Flags\n%s", YEL, NRM);
    printf("   *   -t\tPrint Token list\n");
    printf("   *   -s\tPrint Symbol Table\n");
    printf("   *   -m\tPrint Machine Code\n");
    printf("   *   -a\tPrint Disassembled Code\n");
    printf("   *   -v\tPrint Stack trace\n");

    printf("\n");

    exit(0);
  }

  for(i = 1; i < argc; i++) {
    if(argv[i][0] == '-') {
      switch(argv[i][1]) {
        case 't':
          arguments.t = true;
          break;
        case 's':
          arguments.s = true;
          break;
        case 'm':
          arguments.m = true;
          break;
        case 'a':
          arguments.a = true;
          break;
        case 'v':
          arguments.v = true;
          break;
        default:
          error(
          "Invalid command line arguments. Run './a.out --help' for more help."
          );
          break;
      }
    } else if(arguments.file == NULL) {
      arguments.file = argv[i];
    } else {
      arguments.file = NULL;
      break;
    }
  }

  return arguments;
}

// -----[ verbos ]--------------------------------------------------------------
void verbos(char* file) {

  FILE* in = fopen(file, "r");
  char currentChar;

  printf("// -----[ %*s%*s ]-----------------------------------------------\n",
  (int)(10+strlen(file)/2),file, (int)(10-strlen(file)/2), "");
  while((currentChar = fgetc(in)) != EOF) {
    printf("%c", currentChar);
  }

  printf("\n");
}

// -----[ main ]----------------------------------------------------------------
int main(int argc, char **argv) {

  arguments arguments = args(argc, argv);

  if(arguments.file == NULL)
  error("Invalid command line arguments. Run './a.out --help' for more help.");

  scannerMain(arguments.file);
  parserMain();
  vmMain();

  if(arguments.t) verbos("tokenlist.txt");
  if(arguments.s) verbos("symboltable.txt");
  if(arguments.m) verbos("mcode.txt");
  if(arguments.a) verbos("cleaninput.txt");
  if(arguments.v) verbos("stacktrace.txt");

  return 0;
}
