################################################################################
## Author: Austin Nicholas
##         DaQueshia Irvin
##         Mario Carrasco
##
## PM/0 RUN -- run.sh
################################################################################

########################################
## Compile Programs
########################################
gcc 'SCANNER/scanner.c' -o scanner.run
gcc 'PARSER/parser.c' -o parser.run
gcc 'VM/vm.c' -o vm.run

########################################
## Run Programs
########################################
./scanner.run input.txt
./parser.run tokenlist.txt
./vm.run mcode.txt stacktrace.out

#rm -rf parser.run
rm -rf scanner.run
rm -rf vm.run
