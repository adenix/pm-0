/*******************************************************************************
** Author: Austin Nicholas
**
** PM/0 Virtual Machine -- vm.c v1.0.0
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// -----[ Defines ]-------------------------------------------------------------

// DEBUG
#define DEBUG 1

// dummy bool data type
typedef int bool;
#define true  1
#define false 0

#define MAX_STACK_HEIGHT 2000
#define MAX_CODE_LENGTH 500
#define MAX_LEXI_LEVELS 3

// Command line color codes
#define NRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define YEL  "\x1B[33m"
#define BLU  "\x1B[34m"
#define MAG  "\x1B[35m"
#define CYN  "\x1B[36m"
#define WHT  "\x1B[37m"

// -----[ Global Variables ]----------------------------------------------------

// Operation code Mnemonic array. Provides Order(1) look up for operation names
const char* MNEMONIC[11] = {"lit", "opr", "lod", "sto", "cal", "inc", "jmp",
  "jpc", "sio", "sio", "sio"};

// Max Values for PM/0 Machine
// const int MAX_STACK_HEIGHT = 2000;
// const int MAX_CODE_LENGTH = 500;
// const int MAX_LEXI_LEVELS = 3;

// Stack Declaration in array form
int stack[MAX_STACK_HEIGHT];
// Used to track where lex levels start. Allows ' | ' to be printed
int lex[3];

// Memory array to hold OP L, M in hex form
unsigned memory[MAX_CODE_LENGTH];
unsigned IR = 0;    // Instruction Register

int SP = 0,   // Stack Pointer
    BP = 1,   // Base Pointer
    PC = 0,   // Program Counter
    LC = 0;   // Lexicographical Counter

FILE *fileOut;

// -----[ ALU ]-----------------------------------------------------------------
// Description:     The Arithmentic Logic Unit, or ALU, handles all of the
//                  arithmetic that a program may utilize.
//
void ALU(unsigned M) {

  switch((int) M) {

    // NEG: Pop the stack and push the negation of the result.
    case 1:
      stack[SP] = -stack[SP];
      break;

    // ADD: Pop the stack twice, add the values, and push the result.
    case 2:
      stack[SP-1] = stack[SP] + stack[SP-1];
      SP--;
      break;

    // SUB: Pop the stack twice, subtract the top value from the second value,
    //      and push the result.
    case 3:
      stack[SP-1] = stack[SP-1] - stack[SP];
      SP--;
      break;

    // MUL: Pop the stack twice, multiply the values, and push the result.
    case 4:
      stack[SP-1] = stack[SP] * stack[SP-1];
      SP--;
      break;

    // DIV: Pop the stack twice, divide the second value by the top value, and
    //      push the quotient.
    case 5:
      stack[SP-1] = stack[SP-1] / stack[SP];
      SP--;
      break;

    // ODD: Pop the stack, push 1 if the value is odd, and push 0 otherwise.
    case 6:
      stack[SP] = (stack[SP] % 2) ? 0 : 1;
      break;

    // MOD: Pop the stack twice, divide the second value by the top value, and
    //      push the remainder.
    case 7:
      stack[SP-1] = stack[SP-1] % stack[SP];
      SP--;
      break;

    // EQL: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s = t and 0 otherwise.
    case 8:
      stack[SP-1] = (stack[SP] == stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // NEQ: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s ≠ t and 0 otherwise.
    case 9:
      stack[SP-1] = (stack[SP] != stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // LSS: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s < t and 0 otherwise.
    case 10:
      stack[SP-1] = (stack[SP] > stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // LEQ: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s ≤ t and 0 otherwise.
    case 11:
      stack[SP-1] = (stack[SP] >= stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // GTR: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s > t and 0 otherwise.
    case 12:
      stack[SP-1] = (stack[SP] < stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // GEQ: Pop the stack twice and compare the top value t with the second
    //      value s.  Push 1 if s ≥ t and 0 otherwise.
    case 13:
      stack[SP-1] = (stack[SP] < stack[SP-1]) ? 1 : 0;
      SP--;
      break;

    // Invalid ALU Operation.
    default:
      break;
  }
}

// -----[ base ]----------------------------------------------------------------
// Description:     Given by professor
//
// Returns:         ...
//
int base(l) {

  int b1 = BP; // find base L levels down
  while (l > 0) {
    b1 = stack[b1 + 1];
    l--;
  }
  return b1;
}

// -----[ stackVerbos ]---------------------------------------------------------
// Description:     Print out the stack in a line.
//
void stackVerbos() {

  int i,
      j;
  // Print the stack to the file provided.
  for(i = 1; i <= SP; i++) {

    // Check to Lexicographical Levels and denote them with a '|'
    for(j = 0; j < LC; j++) {
      if(lex[j] == (i-1)) fprintf(fileOut, " |");
    }
    fprintf(fileOut, "%s%d", (i == 1) ? "\t" : " ", stack[i]);
  }
}

// -----[ programImport ]-------------------------------------------------------
// Description:     Import program from text file to MEMORY.
//
// Returns:         True if successful. False if not.
//
bool programImport(char *in){

  FILE *fileIn = fopen(in, "r");

  unsigned  OP,   // Operation Code
            L,    // Lexicographical Level
            M;    // Parameter

  int i;

  while(fscanf(fileIn, "%u", &OP) != EOF) {

    if(fscanf(fileIn, "%u", &L) == EOF) return false;
    if(fscanf(fileIn, "%u", &M) == EOF) return false;

    memory[PC++] = (OP << 12) | (L << 8) | M;
  }

  if (DEBUG) {
    printf("\n");

    printf("%s DEBUG: %sMEMORY\n", YEL, WHT);
    printf("%s   >   |-------------------------------------|%s\n", GRN, NRM);
    printf("%s   >   | MEM ADD |  HEX   |  OP  |  L  |  M  |\n%s", GRN, NRM);
    printf("%s   >   |-------------------------------------|%s\n", GRN, NRM);
    for(i = 0; i < PC; i++)
      printf("%s   >   |   [%2d]  | 0x%.4X |  %2d  |  %d  |  %2d |\n%s", GRN,
        i, memory[i], (memory[i] >> 12), (memory[i] >> 8) & 0x0F,
        memory[i] & 0x00FF, NRM);
    printf("%s   >   |-------------------------------------|%s\n", GRN, NRM);

    printf("\n");
  }

  fclose(fileIn);
  return true;
}

// -----[ verifyArgs ]----------------------------------------------------------
// Description:     Takes in arguments from the command line and verfies that
//                  the arguments are correctly formatted.
//
// Returns:         True if the arguments are formated correctly. False if not
//
bool verifyArgs(int argc, char **argv) {

  if(argc != 3) {

    printf("\n");

    if(argc >= 2 && !strcmp(argv[1], "--help")) {

      printf("%sPM/0: %sTo run vm.c use the following syntax\n\n%s", YEL, WHT, NRM);
      printf("%s   >   ./a.out [INPUT] [OUTPUT]\n%s", GRN, NRM);
      printf("%s   *   [INPUT] - text file formated to contain three numbers seperated by a space on each line.\n%s", CYN, NRM);
      printf("%s   *   [OUTPUT] - text file for the stack trace to be written to.\n%s", CYN, NRM);

    } else {

      printf("%sERROR: %sCommand line arguments syntax is incorrect\n", YEL, WHT);
      printf("       Please retry with the following format..\n\n");
      printf("%s   >   ./a.out [INPUT] [OUTPUT]\n\n%s", GRN, NRM);
      printf("%s   *   for help run './a.out --help'%s", CYN, NRM);

    }

    printf("\n\n");

    return false;
  }

  return true;
}

// -----[ main ]----------------------------------------------------------------
int main(int argc, char **argv) {

  int i;

  if(!verifyArgs(argc, argv)) return 0;
  if(!programImport(argv[1])) return 0;

  // Open output file to be written to
  fileOut = fopen(argv[2], "w");

  // Write program header
  fprintf(fileOut, "Line\tOP\tL\tM\n");

  // Write program code
  for(i = 0; i < PC; i++) {
    fprintf(fileOut, "%2d\t%s\t%d\t%d\n", i, MNEMONIC[(memory[i] >> 12) - 1],
      (memory[i] >> 8) & 0x0F, memory[i] & 0x00FF);
  }

  PC = 0;

  // Write trace header
  fprintf(fileOut, "\n\t\t\t\tpc\tbp\tsp\tstack\n");
  fprintf(fileOut, "Initial values\t\t\t%d\t%d\t%d\n", PC, BP, SP);

  // Run program untill SIO 0, 3 is encountered AKA 0xB003
  while((IR = memory[PC++]) != 0xB003) {

    fprintf(fileOut, "%2d\t%s\t%d\t%d", (PC-1), MNEMONIC[(IR >> 12) - 1],
      (IR >> 8) & 0x0F, IR & 0x00FF);

    switch(IR >> 12) {

      // LIT 0, M:  Push the literal value M onto the stack.
      case 1:
        stack[++SP] = IR & 0x00FF;
        break;

      // OPR 0, 0:  Return from a procedure call.
      // OPR 0, M:  Perform an ALU operation, specified by M.
      case 2:
        if((IR & 0x00FF) == 0) {
          SP = BP-1;
          PC = stack[SP + 4];
          BP = stack[SP + 3];
          LC--;
        } else {
          ALU(IR & 0x00FF);
        }
        break;

      // LOD L, M:  Read the value at offset M from L levels down (if L=0, our
      //            own frame) and push it onto the stack.
      case 3:
        stack[++SP] = stack[base((IR >> 8) & 0x0F) + (IR & 0x00FF)];
        break;

      // STO L, M:  Pop the stack and write the value into offset M from L
      //            levels down – if L=0, our own frame.
      case 4:
        stack[base((IR >> 8) & 0x0F) + (IR & 0x00FF)] = stack[SP--];
        break;

      // CAL L, M:  Call the procedure at M.
      case 5:
        lex[LC++] = SP;
        stack[SP+1] = 0;
        stack[SP+2] = base((IR >> 8) & 0x0F);
        stack[SP+3] = BP;
        stack[SP+4] = PC;
        BP = SP + 1;
        PC = IR & 0x00FF;
        break;

      // INC 0, M:  Allocate enough space for M local variables.  We will always
      //            allocate at least four.
      case 6:
        SP = SP + (IR & 0x00FF);
        break;

      // JMP 0, M:  Branch to M.
      case 7:
        PC = (IR & 0x00FF);
        break;

      // JPC 0, M:  Pop the stack and branch to M if the result is 0.
      case 8:
        if (stack[SP--] == 0) {
          PC = (IR & 0x00FF);
        }
        break;

      // SIO 0, 1:  Pop the stack and write the result to the screen.
      case 9:
        printf("%d\n", stack[SP--]);
        break;

      // SIO 0, 2:  Read an input from the user and store it at the top of the
      //            stack.
      case 10:
        scanf("%d", &stack[SP++]);
        break;

      //
      default:
        break;
    }

    // Write PC BP and SP values
    fprintf(fileOut, "\t%d\t%d\t%d", PC, BP, SP);
    // Write stack
    stackVerbos();
    fprintf(fileOut, "\n");
  }

  fprintf(fileOut, "%2d\t%s\t%d\t%d", (PC-1), MNEMONIC[(IR >> 12) - 1],
    (IR >> 8) & 0x0F, IR & 0x00FF);
  fprintf(fileOut, "\n");

  fclose(fileOut);
  return 0;
}
