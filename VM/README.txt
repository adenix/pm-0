********************************************************************************
** Author: Austin Nicholas
**
** PM/0 Virtual Machine -- README.txt
********************************************************************************

### RUNNING PM/0 VM

To run vm.c use the following syntax
   >   gcc vm.c
   >   ./a.out [INPUT] [OUTPUT]
   *   [INPUT] -  text file formated to contain three numbers separated by a
                  space on each line.
   *   [OUTPUT] - text file for the stack trace to be written to.

### DEBUG MODE

Debug mode will print what memory looks line to the command line in the
following format.

DEBUG: MEMORY
   >   |-------------------------------------|
   >   | MEM ADD |  HEX   |  OP  |  L  |  M  |
   >   |-------------------------------------|
   >   |   [ 0]  | 0x700A |   7  |  0  |  10 |
   >   |   [ 1]  | 0x7002 |   7  |  0  |   2 |
   >   |   [ 2]  | 0x6006 |   6  |  0  |   6 |
   >   |   [ 3]  | 0x100D |   1  |  0  |  13 |
   >   |   [ 4]  | 0x4004 |   4  |  0  |   4 |
   >   |   [ 5]  | 0x1001 |   1  |  0  |   1 |
   >   |   [ 6]  | 0x4104 |   4  |  1  |   4 |
   >   |   [ 7]  | 0x1007 |   1  |  0  |   7 |
   >   |   [ 8]  | 0x4005 |   4  |  0  |   5 |
   >   |   [ 9]  | 0x2000 |   2  |  0  |   0 |
   >   |   [10]  | 0x6006 |   6  |  0  |   6 |
   >   |   [11]  | 0x1003 |   1  |  0  |   3 |
   >   |   [12]  | 0x4004 |   4  |  0  |   4 |
   >   |   [13]  | 0x1000 |   1  |  0  |   0 |
   >   |   [14]  | 0x4005 |   4  |  0  |   5 |
   >   |   [15]  | 0x5002 |   5  |  0  |   2 |
   >   |   [16]  | 0xB003 |  11  |  0  |   3 |
   >   |-------------------------------------|

At the top of the 'Defines' section in vm.c there is the following line of code:
   13 // DEBUG
   14  #define DEBUG 0

Change the '0' to a '1' to run the program in Debug mode.
